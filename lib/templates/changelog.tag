<dblsqd-changelog>
    <div class="dblsqd-changelog-entry" each={version in opts.feed.releases}>
        <h2>{version.version}</h2>
        <virtual each={p in split(version.changelog)}>
            <p if={parent.parent.opts.paragraphHighlight}>
                <strong>{splitParagraph(p)[1]}</strong>
                <span>{splitParagraph(p)[2]}</span>
            </p>
            <p if={!parent.parent.opts.paragraphHighlight}>
                {p}
            </p>
        </virtual>
    </div>
    <div class="dblsqd-branding" if={!opts.hideBranding}>
        <a href="https://www.dblsqd.com">Powered by DBLSQD</a>
    </div>
    <script>
        this.split = function(changelog) {
            return changelog.split("\n")
        }
        this.splitParagraph = function(p) {
            const exp = opts.paragraphHighlightExp || /([^:]*:)?(.*$)/
            return exp.exec(p)
        }
    </script>
    <style>
        .dblsqd-changelog-entry + .dblsqd-changelog-entry {
            margin-top: 2em;
        }
        .dblsqd-branding {
            margin-top: 1.5em;
            font-size: 75%;
        }
    </style>
</dblsqd-changelog>
