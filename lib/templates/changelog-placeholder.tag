<dblsqd-changelog-placeholder>
    <div class="dblsqd-changelog-entry">
        <h2></h2>
        <p></p>
        <p></p>
    </div>
    <div class="dblsqd-changelog-entry">
        <h2></h2>
        <p></p>
        <p></p>
    </div>
    <div class="dblsqd-branding" if={!opts.hideBranding}>
        <a href="https://www.dblsqd.com">Powered by DBLSQD</a>
    </div>
    <script>
    <style>
        @keyframes loading {
            0% {
                background-position: -300px;
            }
            100% {
                background-position: -100%;
            }
        }
        h2, p {
            height: 1em;
            position: relative;
            background-image: linear-gradient(to right, #eee 0, #ddd 150px, #eee 300px);
            animation: loading 1000ms linear infinite;
            animation-timing-function: ease-in;
            background-size: 200%;
        }
        h2:after, p:after {
            display: block;
            content: "";
            position: absolute;
            left: 10em;
            right: 0;
            top: 0;
            bottom: 0;
            background: white;
        }
        p:after {
            left: 30em;
        }
        .dblsqd-changelog-entry + .dblsqd-changelog-entry {
            margin-top: 2em;
        }
        .dblsqd-branding {
            margin-top: 1.5em;
            font-size: 75%;
        }
    </style>
</dblsqd-changelog-placeholder>
