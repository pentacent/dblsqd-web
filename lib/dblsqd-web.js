"use strict"

/**
 * @module dblsqdWeb
 * @desc A module for integrating DBLSQD Feeds into websites.
*/

const Promise = require("promise-polyfill")
const Riot = require("riot")
const changelogTemplate = require("./templates/changelog.tag")
const changelogPlaceholderTemplate = require("./templates/changelog-placeholder.tag")
const defaultEndpoint = "https://feeds.dblsqd.com"

var cache = {}

/**
 * Loads a feed and returns a Promise.
 * A Promise polyfill is included for older browsers.
 * If the feed was retrieved successfully, the Promise is resolved with an
 * object representing the JSON feed.  
 * @alias module:dblsqdWeb.loadFeed
 * @param {string} appToken
 * @param {string} channel
 * @param {string} os
 * @param {string} arch
 * @param {string} [type]
 * @param {object} [opts]
 * @param {string} [opts.endpoint="https://feeds.dblsqd.com"] The Feed endpoint
 * @returns {Promise}
 */
function loadFeed(appToken, channel, os, arch, type, opts) {
    return new Promise((resolve, reject) => {
        opts = opts || {}
        type = type ? "?t=" + type : null
        const now = (new Date()).valueOf()
        const endpoint = opts.endpoint || defaultEndpoint
        const url = [endpoint, appToken, channel, os, arch, type]
            .filter(part => !!part)
            .join("/")
        const cached = cache[url]
        if (cached && cached.expires >= now) {
            resolve(cached.feed)
            return
        }

        const request = new XMLHttpRequest()
        request.open("GET", url, true)
        request.addEventListener("load", response => {
            try {
                const feed = JSON.parse(request.responseText)
                cache[url] = {
                    feed: feed,
                    expires: now + 30 * 1000
                }
                resolve(feed)
            } catch(e) {
                reject(e)
            }
        })
        request.addEventListener("error", error => {
            reject(error)
        })
        request.send()
    })
}

/**
 * Inserts the changelog for a given feed into a DOM element.
 * 
 * @alias module:dblsqdWeb.insertChangelog
 * @param {string|HTMLElement} element
 * @param {object} feed
 * @param {boolean} [opts.paragraphHighlight=false] Highlights a portion of
 * every paragraph by wrapping it in a <strong> tag.
 * @param {RegExp} [opts.paragraphHighlightExp=/([^:]*:)?(.*$)/] The RegExp to
 * determine which part of the paragraph should be highlighted. The RegExp must
 * have two capture groups. The contents of the first group will be highlighted.
 */
function insertChangelog(element, feed, opts) {
    opts = opts || {}
    Riot.mount(element, changelogTemplate, {
        feed: feed,
        paragraphHighlight: opts.paragraphHighlight || !!opts.paragraphHighlightExp,
        paragraphHighlightExp: opts.paragraphHighlightExp
    })
}

/**
 * Inserts an animated placeholder for the changelog into a DOM element.
 * This is usually not necessary since the DBLSQD Feed server is rather fast.
 * @alias module:dblsqdWeb.insertChangelogPlaceholder
 * @param {string|HTMLElement} element
 */
function insertChangelogPlaceholder(element, opts) {
    Riot.mount(element, changelogPlaceholderTemplate, {
    })
}

/**
 * Returns the version number of the latest release in the given feed.
 * @alias module:dblsqdWeb.getLatestVersion
 * @param {object} feed
 * @returns {string}
 */
function getLatestVersion(feed) {
    return feed.releases[0].version
}

/**
 * Inserts the version number of the latest release from the given feed into a
 * DOM element.
 * @alias module:dblsqdWeb.insertLatestVersion
 * @param {string|HTMLElement} element
 * @param {object} feed
 */
function insertLatestVersion(element, feed) {
    const version = getLatestVersion(feed)
    element = typeof element === "string" ?
        document.querySelector(element) :
        element
    element.innerHTML = version
}

function getRelease(feed, version) {
    version = version || getLatestVersion(feed)
    return feed.releases.find(release => release.version === version)
}

/**
 * Returns the download URL for a release from the given feed.
 * If no version is specified, the latest release is used.
 * @alias module:dblsqdWeb.getDownloadUrl
 * @param {object} feed
 * @param {string} [version]
 * @returns {string}
 */
function getDownloadUrl(feed, version) {
    const release = getRelease(feed, version)
    return release ?
        release.download.url :
        undefined
}

/**
 * Returns the SHA256 hash of the download for a release from the given feed.
 * If no version is specified, the latest release is used.
 * @alias module:dblsqdWeb.getDownloadSha256
 * @param {object} feed
 * @param {string} [version]
 * @returns {string}
 */
function getDownloadSha256(feed, version) {
    const release = getRelease(feed, version)
    return release ?
        release.download.sha256 :
        undefined
}

/**
 * Returns the file size in bytes of the download for a release from the given feed.
 * If no version is specified, the latest release is used.
 * @alias module:dblsqdWeb.getDownloadSize
 * @param {object} feed
 * @param {string} [version]
 * @returns {string}
 */
function getDownloadSize(feed, version) {
    const release = getRelease(feed, version)
    return release ?
        release.download.size :
        undefined
}

module.exports = {
    loadFeed: loadFeed,
    insertChangelog: insertChangelog,
    insertChangelogPlaceholder: insertChangelogPlaceholder,
    insertLatestVersion: insertLatestVersion,
    getLatestVersion: getLatestVersion,
    getDownloadUrl: getDownloadUrl,
    getDownloadSha256: getDownloadSha256,    
    getDownloadSize: getDownloadSize    
}
