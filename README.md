#DBLSQD Web Integration

This module allows for the integration of information from DBLSQD feeds into websites.

## Installing

When using browserify (or a similar tool), you can install `dblqsd-web` with npm: `npm install --save-dev dblsqd-web`. Alternatively, you can also directly embed the minified JavaScript file in your site:
```html
<script src="https://unpkg.com/dblsqd-web@0/dist/dblsqd-web.js"></script>
```

## Compatibility

`dblsqd-web` is compatible with all common browsers. IE9 is the oldest supported version of Internet Explorer. 

## Usage

Here is a short example of how to insert an Application’s changelog and latest
version number into a website.

Please take a look at our
[full documentation](https://www.dblsqd.com/docs/integration/web) on the
[DBLSQD](https://www.dblsqd.com) website.

```javascript
dblsqdWeb.loadFeed(":appToken", ":channel", ":os", ":arch").then(function(feed) {
    dblsqdWeb.insertLatestVersion("#download-button span.version-number", feed)
    dblsqdWeb.insertChangelog("#changelog-container", feed)
})
```
