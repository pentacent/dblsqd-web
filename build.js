"use strict"

const browserify = require("browserify")
const babelify = require("babelify")
const uglifyify = require("uglifyify")
const riotify = require("riotify")
const fs = require("fs")

function bundleBase() {
    return browserify({standalone: "dblsqd-web"})
		.add("lib/dblsqd-web.js")
		.transform(riotify)
		.transform(babelify, {
            presets: {
                env: {
                    targets: {
                        ie: 9
                    }
                }
            }
        })
}

bundleBase()
    .bundle()
    .pipe(fs.createWriteStream("dist/dblsqd-web.js"))

bundleBase()
    .transform(uglifyify, {global: true})
    .bundle()
    .pipe(fs.createWriteStream("dist/dblsqd-web.min.js"))
